# webpack4

Webpack 4 Tuts - https://webpack.js.org/

# Memoria de Desarollo

## Requisitos

- Node ^10.16.0
- NPM ^6.9.0

# Ejecución

``` bash

npm init

npm install webpack --save-dev --save-exact

npm install webpack-cli --save-dev --save-exact

npx webpack --entry ./index.js --output ./bundle.js
npx webpack --entry ./index.js --output ./bundle.js --mode development

npm run
npm run build

npm install --save-dev --save-exact css-loader
npm install --save-dev --save-exact style-loader

npm run build:dev -- -w

npm install -D --save-exact webpack-dev-server

npm run build:dev
```

# Plugins

- https://github.com/jantimon/html-webpack-plugin
- https://github.com/webpack-contrib/mini-css-extract-plugin

``` bash
npm install mini-css-extract-plugin --save-dev --save-exact
npm install mini-css-extract-plugin html-webpack-plugin --save-dev --save-exact
``` 
