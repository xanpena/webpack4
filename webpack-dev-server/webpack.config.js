const path = require('path')
const MiniCSSExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require('webpack')

module.exports = {
    entry: {
        home: path.resolve(__dirname, 'src/js/index.js')
    },
    mode: 'development',
    output: {
        //path: path.resolve('output', 'js')
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js'
    },
    devServer: {
        hot:true,
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    //'style-loader',
                    {
                        loader: MiniCSSExtractPlugin.loader
                    },
                    'css-loader'
                ]
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin();
        new HtmlWebpackPlugin({
            title: 'Plugins'
        }),
        new MiniCSSExtractPlugin({
            filename: 'css/[name].css'
        })        
    ]
}